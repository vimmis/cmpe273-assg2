The assignment requirements are:
https://github.com/sithu/cmpe273-fall16/tree/master/assignment2

Submission:
Run below to have mysql and redis running before apps can connect to them.
--name and -d values shouldnt be changed as they are used in apps and proxy for linking.Please change other paths/pwd as per your env.

docker run --name redis -v /docker/host/dir:/data -d redis   

docker run --name mysql -e MYSQL_ROOT_PASSWORD="Chetu1234" -v /home/vimmis/mysqldata:/var/lib/mysql -d mysql:latest  


app1,app2,app3 are 3 folder for apps with different ports to be dockerized seperately.Any addition of app, needs port changes in its files.
change pwd and other env related changes in app files.

You need to go in each folder to dockerize it and run it. docker-compose up command can also be used after build(as no other service added apart from app itself in yml)

proxy folder contains its related files + cb.py, again port is hardcoded and any changes to it, requires changes to other related files.
docker-compose up command can be used(as no other service added apart from proxy itself in yml) and permission taken from professor as its still a seperate container.


Working of proxy:

Throw server error-500 if no app server running/connected/registered or if the current server is not running and circuit breaker is yet not open for it.
Once circuit breaker open, delete from redis and reroute same request to other server.
A snapshot of all working is also posted.

