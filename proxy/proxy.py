import socket
import select
import time
import sys
import redis
import cb

MY_EXCEPTION = 'Dependency Exception'

# Changing the buffer_size and delay, you can improve the speed and bandwidth.
buffer_size = 4096
delay = 0.0001

r = redis.StrictRedis(host='redis', port=6379, db=0)

Flag = False # to check if a server is down or cb is open for it
forward_to = []
maxl = 0

class Forward:
	def __init__(self):
		self.forward = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

	@cb.CircuitBreaker()
	def start(self, host, port):
		try:
			self.forward.connect((host, port))
			return self.forward
		except:
			raise MY_EXCEPTION
			

class TheServer:
	input_list = []
	channel = {}
	global Flag
	global forward_to
	global maxl

	count =0 # to allow round robin
		
	def __init__(self, host, port):
		self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		self.server.bind((host, port))
		self.server.listen(200)
	

	def main_loop(self):
		global Flag
		self.input_list.append(self.server)
		while 1:
			time.sleep(delay)
			ss = select.select
			inputready, outputready, exceptready = ss(self.input_list, [], [])
			for self.s in inputready:
				if self.s == self.server:
					try:					
						self.on_accept()
						# in case of any server down or CB open
						if Flag:  
							# if cb open-set count for latest redis data,call on_accept() to redirect request
							if int (forward_to[self.count]) not in cb.port_cb_map.keys():
								Flag = False
								self.count = (self.count+1)%len((r.hgetall("servers")).keys())
								self.on_accept()
								break
							# if cb not open then just throw server error for the port
							else:
								Flag = False
								clientsock, clientaddr = self.server.accept()
								clientsock.send('HTTP/1.0 500 Internal Server Error\r\n')
								clientsock.close()
								self.count = (self.count+1)%len((r.hgetall("servers")).keys())
		        			break
					# if no app server up for proxy
					except:
						clientsock, clientaddr = self.server.accept()
						clientsock.send('HTTP/1.0 500 Internal Server Error\r\n')
						clientsock.send('Content-Type: text/html\r\n\r\n')
						clientsock.send('No server running, please try again later')	
						clientsock.close()
					
				else:
					self.data = self.s.recv(buffer_size)
					if len(self.data) == 0:
						self.on_close()
						break
					else:
						self.on_recv()

	def on_accept(self):
		global Flag
		global forward_to
		servers = r.hgetall("servers")
		forward_to = servers.keys()
		if len(forward_to)==0: # raise flag for except in main loop
			raise "FLAG" 
		maxl = len(forward_to)
		
		if int(forward_to[self.count]) in  cb.port_cb_map.keys():
			a = cb.port_cb_map[int(forward_to[self.count])]
		else:
			cbo= cb.CircuitBreaker()
			cb.port_cb_map[int(forward_to[self.count])]=cbo
		 
		forward = Forward().start(servers[(forward_to[self.count])],int(forward_to[self.count]))
				
		if forward:
			clientsock, clientaddr = self.server.accept()
			self.count = (self.count+1)%maxl
			print clientaddr, "has connected"
			self.input_list.append(clientsock)
			self.input_list.append(forward)
			self.channel[clientsock] = forward
			self.channel[forward] = clientsock
	    	else:	
			Flag = True
				
	def on_close(self):
		print self.s.getpeername(), "has disconnected"
		#remove objects from input_list
		self.input_list.remove(self.s)
		self.input_list.remove(self.channel[self.s])
		out = self.channel[self.s]
		# close the connection with client
		self.channel[out].close()  # equivalent to do self.s.close()
		# close the connection with remote server
		self.channel[self.s].close()
		# delete both objects from channel dict
		del self.channel[out]
		del self.channel[self.s]
	
	def on_recv(self):
		data = self.data
		# here we can parse and/or modify the data before send forward
		self.channel[self.s].send(data)

if __name__ == '__main__':
	server = TheServer('', 9090)
        try:
		server.main_loop()
        except KeyboardInterrupt:
		print "Ctrl C - Stopping server"
		sys.exit(1)
